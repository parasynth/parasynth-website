import BigNumber from 'bignumber.js';

export default {
  prepareOrder(order, currencies) {
    const [baseCoin, quotedCoin] = order.pair.split('_');
    const amount = new BigNumber(order.quantity);
    const preparedAmount = amount.shiftedBy(-order.baseDecimals).toNumber();
    const executed = new BigNumber(order.executed);
    const preparedExecuted = executed.shiftedBy(-order.baseDecimals).toNumber();

    const perparedOrder = {
      id: order.orderId,
      timestamp: +order.timestamp,
      price: order.price,
      amount: preparedAmount,
      side: order.side,
      type: order.type,
      executed: preparedExecuted,
      asset: {
        symbol: baseCoin,
        title: currencies.find((i) => i.symbol === baseCoin).title,
        pair: `${baseCoin}/${quotedCoin}`,
      },
      commission: order.commission,
      executionPrice: order?.executionPrice ?? '0',
      change24: order?.change24h ?? null,
      high: order?.high24h ?? null,
      low: order?.low24h ?? null,
      volume24: order?.volume24h ?? null,
      status: order.status,
      server: order,
    };

    return perparedOrder;
  },
  prepareOrders(arr, currencies) {
    return arr.reduce((acc, order) => {
      const preparedOrder = this.prepareOrder(order, currencies);
      acc[preparedOrder.id] = preparedOrder;
      return acc;
    }, {});
  },
};
