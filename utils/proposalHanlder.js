import BigNumber from 'bignumber.js';

export const conversionVotingResultinPercent = (supportFor, supportAgainst) => {
  const results = {};
  const supportForBn = new BigNumber(supportFor);
  const totalPercent = new BigNumber(100);
  const total = supportForBn.plus(supportAgainst);

  results.prosPercent = total > 0 ? totalPercent.multipliedBy(supportFor).dividedBy(total).toFixed(2, 1) : '0'; // toFixed ROUND DOWN
  results.consPercent = total > 0 ? totalPercent.multipliedBy(supportAgainst).dividedBy(total).toFixed(2, 1) : '0';
  results.supportFor = supportFor;
  results.supportAgainst = supportAgainst;

  return results;
};

export const calculateEndBlockNumber = (startTime, endTime, startBlockNumber) => {
  const newBlocksPerMin = 3;
  const start = new Date(startTime).getTime();
  const end = new BigNumber(new Date(endTime).getTime());
  const diffMs = end.minus(start);
  const diffMin = diffMs.dividedBy(1000).dividedBy(60);
  const blocksPerProposal = diffMin.dividedBy(newBlocksPerMin);
  return blocksPerProposal.plus(startBlockNumber).toFixed(0);
};
