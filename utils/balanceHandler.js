import BigNumber from 'bignumber.js';

export default {
  prepareBalance(balance) {
    const amount = new BigNumber(balance.balance);
    const preparedAmount = amount.shiftedBy(-balance.currency.decimals);
    const usdValue = preparedAmount.multipliedBy(balance.price);

    const prepareBalance = JSON.parse(JSON.stringify(balance));
    prepareBalance.title = balance.symbol;
    prepareBalance.name = balance.symbol;
    prepareBalance.amount = preparedAmount.toString();
    prepareBalance.usdValue = usdValue.toString();

    if (balance.collateral) {
      prepareBalance.maintanceCollateral = balance.collateral;
    }

    return prepareBalance;
  },
  cacluateTotalBalance(wallets) {
    return Object.values(wallets).reduce((total, wallet) => {
      const usdValue = new BigNumber(wallet.usdValue);
      total = usdValue.plus(total).toNumber();
      return total;
    }, 0);
  },
  prepareBalances(arr) {
    return arr.reduce((acc, balance) => {
      const preparedBalance = this.prepareBalance(balance);
      acc[!balance.isCollateral ? `${preparedBalance.symbol}_` : preparedBalance.symbol] = preparedBalance;
      return acc;
    }, {});
  },
};
