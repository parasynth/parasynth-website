import BigNumber from 'bignumber.js';

/* eslint-disable import/prefer-default-export */
export const calcStakingRewards = (totalRewards, totalStaked, userStaked) => {
  const totalRewardsBn = new BigNumber(totalRewards);
  const userRewards = totalRewardsBn.multipliedBy(userStaked).dividedBy(totalStaked);
  const stakingRewards = userRewards.multipliedBy(100).dividedBy(totalRewards);

  return stakingRewards.toNumber();
};
