export const colors = {
  stroke: '#FF7A00',
  black: '#000000',
  red: '#EB5757',
  green: '#27AE60',
};

export const basicOptions = {
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  responsive: true,
  title: {
    display: false,
  },
  tooltips: {
    enabled: false,
  },
};
export const lineChartOptionsBlue = {
  ...basicOptions,
  responsive: true,
  scales: {
    yAxes: [
      {
        display: false,
      },
    ],
    xAxes: [
      {
        display: false,
      },
    ],
  },
};
