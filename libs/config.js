import addresses from '~/libs/contractAddresses.json';

const infura = process.env.INFURA;
const chainId = process.env.CHAIN_ID;
const swap = process.env.SWAP_CONTRACT_ADDRESS;
const stake = process.env.STAKE_CONTRACT_ADDRESS;
const dao = process.env.DAO_CONTRACT_ADDRESS;
const tokens = addresses.tokens;

console.log('PROCESS.ENV','swap: ', process.env.SWAP_CONTRACT_ADDRESS, 'stake: ', process.env.STAKE_CONTRACT_ADDRESS, 'dao: ', process.env.DAO_CONTRACT_ADDRESS);

const config = {
  infura,
  chainId,
  swap,
  stake,
  dao,
  tokens,
};
export default config;
