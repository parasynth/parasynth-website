import swap from './swap';
import stake from './stake';
import dao from './dao';
import erc20 from './erc20';

export default {
  swap,
  stake,
  dao,
  erc20
}
