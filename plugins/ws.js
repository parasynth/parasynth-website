import Connection from '~/utils/Connection';

export default async ({ store, $axios }, inject) => {
  const connectionManager = {
    availableConnections: [],
    // privateConnections: [],
    connections: [
      {
        name: 'ws', url: process.env.WS_URL, verboseName: 'mainConnection',
      },
    ],
    async initConnection(_connection) {
      const connection = new Connection(_connection.url, _connection.name);
      connection.onConnect = () => {
        store.commit('updateConnectionsStatus', { name: _connection.verboseName, value: true });
      };
      connection.onDisconnect = () => {
        store.commit('updateConnectionsStatus', { name: _connection.verboseName, value: false });
      };
      connection.onError = () => {
        store.commit('updateConnectionsStatus', { name: _connection.verboseName, value: false });
      };
      this.availableConnections.push(connection);
      inject(_connection.name, connection);
    },
    async start() {
      return Promise.all(this.connections.map((item) => this.initConnection(item)));
    },
    async connect(token) {
      return Promise.all(this.availableConnections.map((item) => item.connect(token)));
    },
  };
  await connectionManager.start();
  connectionManager.connect();
  inject('connectionManager', connectionManager);
};
