import { widget } from '~/libs/charting_library.min';

export default (ctx, inject) => {
  inject('charting_library', widget);
};
