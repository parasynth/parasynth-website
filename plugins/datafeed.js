/* eslint-disable class-methods-use-this,no-unused-vars */

function formatId(id) {
  const array = id.split('_');
  return {
    pair: `${array[0]}_${array[1]}`,
    symbol: array[0],
    resolution: array[2],
  };
}
export default (context, inject) => {
  const {
    app, $api, $ws, store,
  } = context;
  class DataFeed {
    constructor(value) {
      this.demoUrl = value;
    }

    onReady(callback) {
      $api.twService.getConfig().then((resp) => {
        callback(resp);
      });
    }

    getServerTime(callback) {
      const time = store.getters['trade/getServerTime'];
      const currentTime = Math.floor(Date.now() / 1000) * 1000;
      const startTime = Date.parse(time);
      const diff = currentTime - startTime;
      const serverTime = startTime + diff;

      callback(Math.floor(serverTime / 1000));
    }

    resolveSymbol(symbolName, onSymbolResolvedCallback, onResolveErrorCallback) {
      const selectedPair = JSON.parse(JSON.stringify(store.getters['trade/getSelectedPair']));

      store.dispatch('trade/fetchPairById', selectedPair.id)
        .then((resolvedSymbol) => {
          resolvedSymbol.has_intraday = true;
          resolvedSymbol.intraday_multipliers = ['1', '5', '15', '60'];
          resolvedSymbol.volume_precision = 18;

          onSymbolResolvedCallback(resolvedSymbol);
        })
        .catch((err) => {
          console.log('ERR RESOLVE SYMBOL: ', err);
          onResolveErrorCallback(err);
        });
    }

    getBars(symbolInfo, resolution, from, to, onHistoryCallback, onErrorCallback, firstDataRequest) {
      const resolutionApi = resolution === 'D' ? '1D' : resolution;
      const params = {
        from: from * 1000,
        to: to * 1000,
      };

      if (firstDataRequest) {
        delete params.to;
      }

      $ws.$get(`/api/tw/history/pair/${symbolInfo.id}/${resolutionApi}/`, params)
        .then((message) => {
          const barsApi = message.payload.result.result;

          if (message.payload.ok === true && barsApi.length) {
            const bars = barsApi.map((bar) => ({
              time: bar.t,
              low: bar.l,
              high: bar.h,
              volume: bar.v,
              open: bar.o,
              close: bar.c,
            }));

            onHistoryCallback(bars);
          } else {
            onHistoryCallback([], { noData: true });
          }
        })
        .catch((err) => {
          onErrorCallback(err);
        });
    }

    async subscribeBars(symbolInfo, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) {
      const selectedPair = store.getters['trade/getSelectedPair'];

      try {
        await $ws.subscribe(`/tw/pair/${selectedPair.id}/`, (message) => {
          const tmpBar = message.data;
          const bar = {
            time: tmpBar.t,
            low: tmpBar.l,
            high: tmpBar.h,
            volume: tmpBar.v,
            open: tmpBar.o,
            close: tmpBar.c,
          };

          onRealtimeCallback(bar);
        });
      } catch (err) {
        console.log('TW PAIR err: ');
        console.dir(err);
      }
    }

    async unsubscribeBars(subscribeUID) {
      const info = formatId(subscribeUID);
      const pairs = store.getters['trade/getPairs'];
      const pairId = pairs.find((i) => i.fullName === info.pair).id;

      $ws.unsubscribe(`/tw/pair/${pairId}/`);
    }
  }
  inject('datafeed', DataFeed);
};
