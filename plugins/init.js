import { initWeb3 } from './web3';

export default async (context, inject) => {
  const { store } = context;
  const isWalletConnected = window.sessionStorage.getItem('isWalletConnected');

  const init = {
    init: () => {
      const currencies = store.dispatch('main/fetchCurrencies');
      const requests = [currencies];

      return Promise.all(requests);
    },
  };

  try {
    store.dispatch('main/setLoading', true);
    await initWeb3();
    await init.init();

    if (isWalletConnected) {
      store.dispatch('wallet/initMetamask');
    }
  } catch (err) {
    console.log('loading', err);
  } finally {
    store.dispatch('main/setLoading', false);
  }

  inject('init', init);
};
