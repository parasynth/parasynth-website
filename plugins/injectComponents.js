import Vue from 'vue';

import BaseModal from '~/components/CtmModal';
import BaseModalBox from '~/components/CtmModal/CtmModalBox';
import BaseBtn from '~/components/ui/base-btn';
import BaseCard from '~/components/ui/base-card';
import BaseTabs from '~/components/ui/base-tabs';
import ButtonRadioGroup from '~/components/ui/button-radio-group';
import BaseSelect from '~/components/ui/base-select';
import BaseInput from '~/components/ui/base-input';
import InputCurrency from '~/components/ui/input-currency';
import BtnLoader from '~/components/ui/btn-loader';
import BaseBanner from '~/components/ui/base-banner';

Vue.component('base-btn', BaseBtn);
Vue.component('base-modal', BaseModal);
Vue.component('base-modal-box', BaseModalBox);
Vue.component('base-card', BaseCard);
Vue.component('base-tabs', BaseTabs);
Vue.component('button-radio-group', ButtonRadioGroup);
Vue.component('base-select', BaseSelect);
Vue.component('base-input', BaseInput);
Vue.component('input-currency', InputCurrency);
Vue.component('btn-loader', BtnLoader);
Vue.component('base-banner', BaseBanner);
