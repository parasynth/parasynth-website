/* eslint-disable import/prefer-default-export */
import Web3, { utils } from 'web3';
import BigNumber from 'bignumber.js';
import Web4 from '@cryptonteam/web4';
import config from '~/libs/config';
import modals from '~/store/modals/modals';
import abi from '~/libs/abi/index';
import { PROPOSAL_TYPES } from '~/utils/constants';

let store;
if (process.browser) {
  window.onNuxtReady(({ $store }) => {
    store = $store;
  });
}

BigNumber.config({ EXPONENTIAL_AT: 60 });

let web3Wallet;
let web3Anon;
let web4Wallet;
let web4Anon;

let userAddress;

let tokenAbstractionWallet;
let tokenAbstractionAnon;
let swapInstance;
let stakeInstance;
let daoInstance;

let stakeInstanceAnon;
let stakingTokenAddress;

export const output = (res) => ({
  ok: true,
  result: res,
});

export const error = (code, msg, err) => {
  const _err = err ?? new Error(msg);
  _err.ok = false;
  _err.code = code;

  throw _err;
};

export const getWeb3Instances = () => {
  const instances = {
    web3Anon,
    web4Anon,
    web3Wallet,
    web4Wallet,
  };

  return instances;
};

export const getTokenAbstractionWallet = () => tokenAbstractionWallet;

export const createTokenWallet = () => {
  if (web4Wallet) tokenAbstractionWallet = web4Wallet.getContractAbstraction(abi.erc20);

  return tokenAbstractionWallet;
};
export const createTokenAnon = () => {
  if (web4Anon) tokenAbstractionAnon = web4Anon.getContractAbstraction(abi.erc20);

  return tokenAbstractionAnon;
};
export const createContractInstace = async (contractAbi, address) => {
  const contractAbstraction = web4Wallet.getContractAbstraction(contractAbi);
  const contractInstance = await contractAbstraction.getInstance(address);

  return contractInstance;
};
export const createContractInstaceAnon = async (contractAbi, address) => {
  const contractAbstraction = web4Anon.getContractAbstraction(contractAbi);
  const contractInstance = await contractAbstraction.getInstance(address);

  return contractInstance;
};

export const voteToProposal = async (id, vote) => {
  console.log('id: ', id);
  console.log('vote: ', vote);
  try {
    await daoInstance.vote(id, vote);
    return output(true);
  } catch (err) {
    console.log('WEB3 voteToProposal err: ');
    console.dir(err);
    return error(500, err.message, err);
  }
};

export const finishVote = async (id) => {
  try {
    await daoInstance.finishVote(id);
    return output(true);
  } catch (err) {
    console.log('WEB3 finishVote err: ');
    console.dir(err);
    return error(500, err.message, err);
  }
};

export const getBlockNumberbyTx = async (txHash) => {
  const transaction = await web3Anon.eth.getTransaction(txHash);

  return transaction?.blockNumber;
};

const getAbiandParamsByProposalType = (type, { address, collateral }) => {
  let jsonAbi = null;
  let params = null;

  if (type === PROPOSAL_TYPES.ADD_TOKEN) {
    jsonAbi = {
      name: 'addToken',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
      inputs: [
        {
          internalType: 'address',
          name: 'token',
          type: 'address',
        },
        {
          internalType: 'int256',
          name: 'price',
          type: 'int256',
        },
        {
          internalType: 'uint256',
          name: 'coll',
          type: 'uint256',
        },
      ],
    };
    params = [address, '1000', collateral];
  }

  return { jsonAbi, params };
};

export const getBytecodeForProposal = (address, type, collateral) => {
  const { jsonAbi, params } = getAbiandParamsByProposalType(type, { address, collateral });

  if (!jsonAbi || !params) return error(500, 'failed to generate bytecode');

  try {
    const byteCode = web3Wallet.eth.abi.encodeFunctionCall(jsonAbi, params);

    return byteCode;
  } catch (err) {
    console.log('BYTECODE err: ');
    console.dir(err);
    return error(err);
  }
};

export const validateAddress = (address) => {
  const isAddress = web3Wallet.utils.isAddress(address);
  console.log('isAddress: ', isAddress);
  return isAddress;
};

export const getTokenInfoByAddress = async (address) => {
  const tokenInstance = await tokenAbstractionWallet.getInstance(address);
  const symbol = await tokenInstance.symbol();
  const name = await tokenInstance.name();
  return { symbol, name };
};

export const getStakingInfo = async () => {
  try {
    const stakingInfo = await stakeInstanceAnon.getStakingInfo();

    stakingTokenAddress = stakingInfo._stakeTokenAddress;

    const stakeToken = await tokenAbstractionAnon.getInstance(stakingTokenAddress);
    const decimals = utils.BN(await stakeToken.decimals()).toNumber();
    const stakedCoins = (new BigNumber(stakingInfo._totalStaked)).shiftedBy(-decimals).toNumber();
    const totalClaimed = (new BigNumber(stakingInfo._totalDistributed)).shiftedBy(-decimals).toNumber();
    const totalRewards = (new BigNumber(stakingInfo._rewardTotal)).shiftedBy(-decimals).toNumber();
    const _stakingInfo = {
      stakedCoins,
      totalClaimed,
      totalRewards,
      stakingRewards: null,
    };

    return _stakingInfo;
  } catch (err) {
    console.log('GENERAL STAKING INFO err: ');
    console.dir(err);
    return error(500, err.message, err);
  }
};

export const claimRewards = async () => {
  try {
    const claimed = await stakeInstance.claim();

    return output({ tx: claimed.tx });
  } catch (err) {
    console.log('CLAIM err: ');
    console.dir(err);
    return error(500, err.message, err);
  }
};

export const unstake = async (amount) => {
  if (!amount) return error(500, 'amount is required');

  try {
    const erc20Instance = await tokenAbstractionWallet.getInstance(stakingTokenAddress);
    const decimals = utils.BN(await erc20Instance.decimals()).toNumber();
    const amountBn = new BigNumber(amount);
    const unstakeAmount = amountBn.shiftedBy(decimals).toString();
    const unstaked = await stakeInstance.unstake(unstakeAmount);

    return output({ tx: unstaked.tx });
  } catch (err) {
    console.log('CLAIM err: ');
    console.dir(err);
    return error(500, err.message, err);
  }
};

export const getStakingInfoByAddress = async (address) => {
  if (!address) return error(500, 'amount is required');

  try {
    const info = await stakeInstance.getInfoByAddress(address);

    const stakeToken = await tokenAbstractionWallet.getInstance(stakingTokenAddress);
    const decimals = utils.BN(await stakeToken.decimals()).toNumber();
    const staked = (new BigNumber(info.staked_)).shiftedBy(-decimals).toNumber();
    const claim = (new BigNumber(info.claim_)).shiftedBy(-decimals).toNumber();
    const totalClaimed = (new BigNumber(info.totalClaim)).shiftedBy(-decimals).toNumber();

    return { staked, claim, totalClaimed };
  } catch (err) {
    console.log('USER STAKING INFO err: ');
    console.dir(err);
    return error(500, err.message, err);
  }
};

export const depositETH = async (toAddress, value) => {
  const valueBn = new BigNumber(value);
  const depositedValue = valueBn.shiftedBy(18);
  const tx = {
    from: userAddress,
    to: toAddress,
    value: depositedValue,
  };

  const txHash = await web3Wallet.eth.sendTransaction(tx);
  console.log('txHash: ', txHash);

  return txHash;
};

export const depositERC0 = async (to, amount) => {
  const decimals = 18;
  const amountBn = new BigNumber(amount);
  const depositedAmount = amountBn.shiftedBy(decimals).toString();
  console.log('depositedAmount: ', depositedAmount);
  const erc20Instance = await tokenAbstractionWallet.getInstance(to);
  const allowance = utils.BN(await erc20Instance.allowance(userAddress, config.swap)).toString();
  console.log('allowance: ', allowance);

  if (+allowance < +depositedAmount) {
    const approve = await erc20Instance.approve(config.swap, depositedAmount);
    console.log('approve: ', approve);
  }

  const depositERC20 = await swapInstance.deposit(depositedAmount, to);
  console.log('depositERC20: ', depositERC20);
  return depositERC20.tx;
};

export const stakeERC0 = async (amount) => {
  if (!amount) return error(500, 'amount is required');

  const erc20Instance = await tokenAbstractionWallet.getInstance(stakingTokenAddress);
  const decimals = utils.BN(await erc20Instance.decimals()).toNumber();
  const amountBn = new BigNumber(amount);
  const stakedAmount = amountBn.shiftedBy(decimals).toString();
  const allowance = utils.BN(await erc20Instance.allowance(userAddress, config.stake)).toString();

  if (+allowance < +stakedAmount) {
    const approve = await erc20Instance.approve(config.stake, stakedAmount);
  }

  const stakeERC20 = await stakeInstance.stake(stakedAmount);
  console.log('stakeERC20: ', stakeERC20);

  return stakeERC20.tx;
};

export const withdrawETH = async (amount) => {
  const decimals = 18;
  const amountBn = new BigNumber(amount);
  const withdrawAmount = amountBn.shiftedBy(decimals).toString();

  console.log('______________WITHDRAW ETH INFO_______________');
  console.log('SWAP Instance: ', swapInstance);
  console.log('withdraw amount: ', withdrawAmount);

  console.log('_____________START WITHDRAW ETH____________');
  const withdrawedETH = await swapInstance.withdrawETH(withdrawAmount);

  console.log('withdrawETH response: ', withdrawedETH);
  console.log('______________END WITHDRAW ETH______________');
  return withdrawedETH.tx;
};

export const withdrawERC0 = async (from, amount) => {
  const decimals = 18;
  const amountBn = new BigNumber(amount);
  const withdrawAmount = amountBn.shiftedBy(decimals).toString();

  console.log('______________WITHDRAW INFO_______________');
  console.log('SWAP Instance: ', swapInstance);
  console.log('withdraw amount: ', withdrawAmount);
  console.log('token address: ', from);

  console.log('_____________START WITHDRWA____________');
  const withdrawERC20 = await swapInstance.withdraw(withdrawAmount, from);

  console.log('withdrawERC20 response: ', withdrawERC20);
  console.log('______________END WITHDRAW______________');
  return withdrawERC20.tx;
};

export const getMetamaskBalance = async () => new BigNumber(await web3Wallet.eth.getBalance(userAddress)).shiftedBy(-18).toString();

export const initProviderWallet = async (address) => {
  console.log('initProviderWallet', address);
  try {
    await window.ethereum.enable();
    web3Wallet = new Web3(window.ethereum);

    let userNetwork;

    await Promise.all([
      userNetwork = (await web3Wallet.eth.net.getId()).toString(),
      userAddress = await web3Wallet.eth.getCoinbase(),
    ]);

    console.log('initProvider', typeof userNetwork, userNetwork, typeof config.chainId, config.chainId);
    if (userNetwork !== config.chainId.toString()) {
      return error(433, 'incorrect network');
    }

    web4Wallet = new Web4();
    await web4Wallet.setProvider(window.ethereum, userAddress);

    const token = createTokenWallet();
    console.log('token: ');
    console.dir(token);
    swapInstance = await createContractInstace(abi.swap, config.swap);
    stakeInstance = await createContractInstace(abi.stake, config.stake);
    daoInstance = await createContractInstace(abi.dao, config.dao);
    console.log('swap: ', config.swap, 'stake: ', config.stake, 'dao: ', config.dao);
    console.log('swapInstance: ', swapInstance, 'stakeInstance: ', stakeInstance, 'daoInstance: ', daoInstance);

    return output({
      address: userAddress,
    });
  } catch (err) {
    if (err.code === 433) throw err;

    return error(500, 'connection error', err);
  }
};

export const initWeb3 = async () => {
  try {
    let infuraUrl;
    if (+config.chainId === 1) {
      infuraUrl = `wss://mainnet.infura.io/ws/v3/${config.infura}`;
    } else {
      infuraUrl = `wss://rinkeby.infura.io/ws/v3/${config.infura}`;
    }
    const wsOptions = {
      reconnect: {
        auto: true,
        delay: 5000, // ms
        maxAttempts: 5,
        onTimeout: false,
      },
    };
    const providerInfura = new Web3.providers.WebsocketProvider(infuraUrl, wsOptions);

    web4Anon = new Web4();
    web3Anon = new Web3(providerInfura);
    web4Anon.setProvider(providerInfura);

    stakeInstanceAnon = await createContractInstaceAnon(abi.stake, config.stake);
    createTokenAnon();

    return output({ web3Anon, web4Anon });
  } catch (err) {
    if (err.code === 433) return err;

    return error(500, 'connection error', err);
  }
};

export const signData = async (message, isOrderCreate = false) => {
  if (!message) {
    return error(500, 'message is required');
  }

  let messageHash = '';

  if (isOrderCreate) {
    messageHash = web3Wallet.utils.soliditySha3('1', message.toString());
  } else {
    messageHash = web3Wallet.utils.soliditySha3(message.toString());
  }

  let signature;
  console.log('messageHash: ', messageHash);
  console.log('userAddress: ', userAddress, typeof userAddress);

  try {
    signature = await web3Wallet.eth.personal.sign(messageHash, userAddress);
  } catch (err) {
    console.log('WEB3 SIGN err: ');
    console.dir(err);
    return error(500, err.message, err);
  }

  return output({ signature, messageHash });
};

export const recover = (messageHash, signature) => {
  web3Wallet.eth.personal.ecRecover(messageHash, signature, (err, res) => {
    if (err) {
      console.log('RECOVER err');
      console.dir(err);
    }
    console.log('RECOVER res', res);
  });
};
