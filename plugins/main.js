import Vue from 'vue';
import moment from 'moment';
import Bignumber from 'bignumber.js';
import VueTippy, { TippyComponent } from 'vue-tippy';
import modals from '~/store/modals/modals';
import { DECIMALS_BY_CURRENCY_TYPE } from '~/utils/constants';

Vue.use(VueTippy);
Vue.component('tippy', TippyComponent);

Vue.mixin({

  methods: {
    async Signer(callback, ...args) {
      const isWalletConnected = this.$store.getters['wallet/getIsConnectedToMetamask'];
      if (isWalletConnected) {
        callback(...args);
      } else {
        await this.ShowModal({
          key: modals.connectWallet,
          title: this.$t('wallet-connection-modal.title'),
          text: this.$t('wallet-connection-modal.description'),
        });

        const timeoutID = setTimeout(() => {
          this.CloseModal();
          clearTimeout(timeoutID);
        }, 3000);
      }
      return isWalletConnected;
    },
    ShowModal(payload) {
      this.$store.dispatch('modals/show', {
        key: modals.default,
        ...payload,
      });
    },
    CloseModal() {
      this.$store.dispatch('modals/hide');
    },
    ShowError(label) {
      this.$bvToast.toast(label, {
        title: this.$t('misc.error'),
        variant: 'warning',
        solid: true,
        toaster: 'b-toaster-bottom-right',
        appendToast: true,
      });
    },
    Floor: (value, precision = 4) => {
      const form = 10 ** precision;
      return Math.floor(value * form) / form || 0;
    },
    Ceil: (value, precision = 4) => {
      const form = 10 ** precision;
      return Math.ceil(value * form) / form || 0;
    },
    GetFormTimestamp(timestamp, format = 'DD.MM.YY H:mm') {
      if (timestamp !== 0 && timestamp !== '-' && timestamp !== '' && timestamp !== undefined) {
        // timestamp = +timestamp * 1000;
        timestamp = +timestamp;
        return moment(new Date(timestamp)).format(format);
      }
      return '-';
    },
    Require(img) {
      // eslint-disable-next-line global-require
      // return require(`assets/img/${img}`);
    },
    NumberFormat(value, fixed) {
      return (+value && new Intl.NumberFormat('ru', { maximumFractionDigits: fixed || 8 }).format(value || 0)) || 0;
    },
    // number localization for crypto
    $crn(value) {
      if (value === null || typeof value === 'undefined') return '-';

      const bnValue = new Bignumber(value);
      const dp = bnValue.dp();
      const minimumFractionDigits = dp < DECIMALS_BY_CURRENCY_TYPE.CRYPTO ? dp : DECIMALS_BY_CURRENCY_TYPE.CRYPTO;
      const options = {
        maximumFractionDigits: DECIMALS_BY_CURRENCY_TYPE.CRYPTO,
        minimumFractionDigits,
      };
      return this.$n(value, options);
    },
    // number localization for fiat
    $fn(value) {
      if (value === null || typeof value === 'undefined') return '-';

      const bnValue = new Bignumber(value);
      const dp = bnValue.dp();
      const minimumFractionDigits = dp > 0 ? DECIMALS_BY_CURRENCY_TYPE.FIAT : dp;
      const options = {
        maximumFractionDigits: DECIMALS_BY_CURRENCY_TYPE.FIAT,
        minimumFractionDigits,
      };
      return this.$n(value, options);
    },
    // number localization for custom
    $cn(value, max, min) {
      if (value === null || typeof value === 'undefined') return '-';

      // get rid of rounding to fraction decimal places
      const _min = typeof min === 'undefined' ? max : min;
      const bnValue = new Bignumber(value);
      const processed = bnValue.toFixed(max, 1); // round down
      const options = {
        maximumFractionDigits: max,
        minimumFractionDigits: _min,
      };

      return this.$n(processed, options);
    },
  },
});
