import httpClient from '../httpClient';

const getNextPollTime = () => httpClient.$get('/cycle/').then((res) => res.result.nextCycle);

export default {
  getNextPollTime,
};
