import httpClient from '../httpClient';

const getPairs = () => httpClient.$get('/pair/')
  .then((res) => {
    const { pairs } = res.result;

    return pairs.map((pair) => {
      pair.name = pair.name.split('_').join('/');
      return pair;
    });
  });

const getPairById = (id) => httpClient.$get(`/pair/${id}/`).then((res) => res.result.pair);

const getPairStatusInfo = (id) => httpClient.$get(`/history/pair/${id}/`).then((res) => res.result);

export default {
  getPairs,
  getPairById,
  getPairStatusInfo,
};
