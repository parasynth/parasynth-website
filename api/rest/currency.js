import httpClient from '../httpClient';

const getCurrencies = () => httpClient.$get('/currency/');

export default {
  getCurrencies,
};
