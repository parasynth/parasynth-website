import httpClient from '../httpClient';

const getHistoryByResolution = (resolution) => httpClient.$get(`/history/${resolution}`);
const getConfig = () => httpClient.$get('/tw/config/').then((res) => res.result.config);

export default {
  getHistoryByResolution,
  getConfig,
};
