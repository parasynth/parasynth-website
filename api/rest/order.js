import httpClient from '../httpClient';

const createOrder = (orderData) => httpClient.$post('/create/order/', orderData);
const getOrders = () => httpClient.$get('/order/');
const getUserOrders = (payload, params) => httpClient.$post('/user/order/', payload, { params });
const cancelOrder = (orderId, signature) => httpClient.$post(`/order/cancel/${orderId}/`, signature);

export default {
  createOrder,
  getUserOrders,
  getOrders,
  cancelOrder,
};
