import httpClient from '../httpClient';

const getStakingTransactions = ({ signature, params }) => {
  const options = {};

  if (params) {
    options.params = params;
  }

  return httpClient.$post('user/stake/', { signature }, options)
    .then((res) => res.result);
};

const fetchTokenHistoricalPrice = async (coinId, date) => {
  const params = {
    date,
    localization: false,
  };

  let historicalPrice = null;
  try {
    const resp = await httpClient.$get(`${process.env.COINGECKO_URL}/coins/${coinId}/history`, { params });
    historicalPrice = resp.market_data.current_price.usd;
  } catch (err) {
    console.log('err historicalPrice: ', err);
    console.dir(err);
  }

  return historicalPrice;
};

const fetchTokenCurrentPrice = async (coinId) => {
  const params = {
    ids: coinId,
    vs_currencies: 'usd',
  };
  let currentPrice = null;

  try {
    const resp = await httpClient.$get(`${process.env.COINGECKO_URL}/simple/price`, { params });

    if (typeof resp[coinId] !== 'undefined') {
      currentPrice = resp[coinId][params.vs_currencies];
    }
  } catch (err) {
    console.log('err currentPrice: ', err);
    console.dir(err);
  }

  return currentPrice;
};

export default {
  getStakingTransactions,
  fetchTokenCurrentPrice,
  fetchTokenHistoricalPrice,
};
