import httpClient from '../httpClient';
import { getBlockNumberbyTx } from '~/plugins/web3';
import { calculateEndBlockNumber, conversionVotingResultinPercent } from '~/utils/proposalHanlder';

const createProposal = (proposal) => httpClient.$post('/create/proposal/', proposal);

const getProposals = (options = null) => httpClient.$get('/proposal/', options)
  .then((res) => res.result);

const getProposalById = (id, signature) => httpClient.$post(`/proposal/${id}/`, { signature })
  .then(async (res) => {
    const { proposal } = res.result;

    const votingResults = conversionVotingResultinPercent(proposal.supportFor, proposal.supportAgainst);
    const startBlockNumber = await getBlockNumberbyTx(proposal.hash);
    const endBlockNumber = calculateEndBlockNumber(proposal.createdAt, proposal.endTimeOfVoting, startBlockNumber);
    const proposalInfo = {
      startBlock: startBlockNumber,
      endBlock: endBlockNumber,
      startTime: proposal.createdAt,
      endTime: proposal.endTimeOfVoting,
      asset: proposal.description,
      txHash: proposal.hash,
      id: proposal.id,
      status: proposal.status,
      type: proposal.type,
      isVoted: res.result.vote,
    };

    proposal.votingResults = votingResults;
    proposal.proposalInfo = proposalInfo;

    return proposal;
  });

export default {
  createProposal,
  getProposals,
  getProposalById,
};
