import httpClient from '../httpClient';

const getBalance = (signature) => httpClient.$post('/user/balance/', signature);

export default {
  getBalance,
};
