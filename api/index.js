import { setHTTPClientInstance } from './httpClient';
import currencyService from './rest/currency';
import orderService from './rest/order';
import pairService from './rest/pair';
import twService from './rest/tw';
import balanceService from './rest/balance';
import tradeService from './rest/trade';
import proposalService from './rest/proposal';
import stakeService from './rest/stake';

export default {
  currencyService,
  orderService,
  pairService,
  twService,
  balanceService,
  tradeService,
  proposalService,
  stakeService,
};

export {
  setHTTPClientInstance,
};
