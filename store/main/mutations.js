export default {
  setLoading(state, status) {
    state.isLoading = status;
  },
  setCurrencies(state, currencies) {
    state.currencies = currencies;
  },
};
