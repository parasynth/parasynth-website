export default {
  setLoading({ commit }, status) {
    commit('setLoading', status);
  },
  async fetchCurrencies({ commit, dispatch }) {
    const currencies = await this.$api.currencyService.getCurrencies();
    const { coin } = currencies.result;
    commit('setCurrencies', coin);
    // TODO: ??
    dispatch('market/updateCurrenciesTable', coin, { root: true });
  },
  async getApiData({ dispatch }) {
    await dispatch('rate/getRate', null, { root: true });
  },
};
