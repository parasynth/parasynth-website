export default {
  getLoadingStatus: (state) => state.isLoading,
  getCurrencies: (state) => state.currencies,
};
