import BigNumber from 'bignumber.js';
import {
  claimRewards, getStakingInfo, getStakingInfoByAddress, unstake,
} from '~/plugins/web3';

export default {
  async getGeneralStakingInfo({ commit, getters }) {
    const userStakingInfo = getters.getUserStakingInfo;
    const stakingInfo = await getStakingInfo();

    commit('setGeneralStakingInfo', stakingInfo);
    if (userStakingInfo) {
      commit('updateGeneralStakingInfo', userStakingInfo);
    }
  },
  async getUserStakingInfo({ commit, rootGetters }) {
    const userAddress = rootGetters['wallet/getUserAddress'];
    const info = await getStakingInfoByAddress(userAddress);
    commit('setUserStakingInfo', info);
    commit('updateGeneralStakingInfo', info);
  },
  async claim({ commit }) {
    const claimedTx = await claimRewards();
    return claimedTx;
  },
  async unstake({ commit }, amount) {
    const unstakeTx = await unstake(amount);
    return unstakeTx;
  },
  async getStakingTransactions({ commit }, { signature, params }) {
    const transactions = await this.$api.stakeService.getStakingTransactions({ signature, params });

    commit('setStakingTransactions', transactions);
  },
  async calculateAPY({ commit }, { coinId, date }) {
    const historicalPrice = await this.$api.stakeService.fetchTokenHistoricalPrice(coinId, date);
    const currentPrice = await this.$api.stakeService.fetchTokenCurrentPrice(coinId);

    if (!currentPrice || !historicalPrice) {
      commit('setAPY', null);
      return null;
    }

    const currentPriceBN = new BigNumber(currentPrice);
    const apy = currentPriceBN.div(historicalPrice).multipliedBy(100).toFixed(2, 1);

    commit('setAPY', apy);
    return apy;
  },
};
