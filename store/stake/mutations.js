import { calcStakingRewards } from '~/utils/stakeHandler';

export default {
  setGeneralStakingInfo(state, stakingInfo) {
    state.stakingInfoGeneral = stakingInfo;
  },
  setUserStakingInfo(state, userStakingInfo) {
    console.log('userStakingInfo: ', userStakingInfo);
    state.userStakingInfo = userStakingInfo;
  },
  updateGeneralStakingInfo(state, userStakingInfo) {
    const copy = JSON.parse(JSON.stringify(state.stakingInfoGeneral));
    copy.stakingRewards = calcStakingRewards(copy.totalRewards, copy.stakedCoins, userStakingInfo.staked);
    state.stakingInfoGeneral = copy;
  },
  setStakingTransactions(state, transactions) {
    state.transactions.data = transactions;
  },
  setAPY(state, apy) {
    state.apy = apy;
  },
};
