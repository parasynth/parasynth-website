export default () => ({
  stakingInfoGeneral: null,
  userStakingInfo: null,
  transactions: {
    count: 0,
    data: [],
  },
  apy: null,
});
