export default {
  getGeneralStakingInfo: (state) => state.stakingInfoGeneral,
  getUserStakingInfo: (state) => state.userStakingInfo,
  getStakingTransactions: (state) => state.transactions.data,
  getApy: (state) => state.apy,
};
