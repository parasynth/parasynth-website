export default {
  setAddress(state, value) {
    state.address = value;
  },
  setIsConnectedToMetamask(state, value) {
    state.isConnectedToMetamask = value;
  },
  setUserSignature(state, stignature) {
    state.signatures.user = stignature;
  },
  setBalances(state, balances) {
    state.balance.wallets = balances.wallets;
    state.balance.excessCollateral = balances.excessCollateral;
    state.balance.collateralCoins = balances.totalCollateral;
    state.balance.totalBalance = balances.totalBalance;
  },
};
