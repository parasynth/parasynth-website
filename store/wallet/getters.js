export default {
  getIsConnectedToMetamask: (state) => state.isConnectedToMetamask,
  getBalance: (state) => state.balance,
  getWallets: (state) => state.balance.wallets,
  getParasynthTokensBalance: (state) => Object.values(state.balance.wallets).filter((i) => !i.isCollateral),
  getCollateralCoins: (state) => Object.values(state.balance.wallets).filter((i) => i.isCollateral),
  getUserSignature: (state) => state.signatures.user,
  getExcessCollateral: ({ balance }) => balance.excessCollateral,
  getUserAddress: (state) => state.address,
};
