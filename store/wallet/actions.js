import config from '~/libs/config';
import {
  initProviderWallet, withdrawETH, depositETH, signData, depositERC0, withdrawERC0, stakeERC0,
} from '~/plugins/web3';
import balanceHandler from '~/utils/balanceHandler';

export default {
  async initMetamask({ commit, dispatch }) {
    const response = await initProviderWallet();
    console.log('response: ', response);
    if (response.ok) {
      commit('setIsConnectedToMetamask', response.ok);
      commit('setAddress', response.result.address);
      window.sessionStorage.setItem('isWalletConnected', true);
    }

    return response;
  },
  async sign(store, payload) {
    const _payload = {
      message: payload.message,
      isOrderCreate: payload?.isOrderCreate ?? false,
    };

    let sign = null;

    try {
      sign = await signData(_payload.message, _payload.isOrderCreate);
    } catch (err) {
      console.log('err: ', err);
      console.dir(err);
      throw err;
    }

    return { signature: sign.result.signature, messageHash: sign.result.messageHash };
  },
  async depositFunds({ commit }, { value, currency }) {
    console.log('currency: ', currency);
    const currencies = ['eth'];
    if (currencies.includes(currency)) {
      const { transactionHash } = await depositETH(config.swap, value);
      return transactionHash;
    }

    const txHash = await depositERC0(currency, value);

    return txHash;
  },
  async stakeFunds({ commit }, value) {
    console.log('stake value: ', value);
    const txHash = await stakeERC0(value);

    return txHash;
  },
  async withdrawFunds({ commit }, { value, currency }) {
    console.log('WITHDRAW currency: ', currency);
    const currencies = ['eth'];
    if (currencies.includes(currency)) {
      const { transactionHash } = await withdrawETH(value);
      return transactionHash;
    }

    const txHash = await withdrawERC0(currency, value);

    return txHash;
  },
  async fetchBalance({ commit }, signature) {
    const res = await this.$api.balanceService.getBalance(signature);

    if (Array.isArray(res.result) && !res.result.length) {
      return;
    }

    const balance = res.result;
    const wallets = balanceHandler.prepareBalances(balance.wallets);
    const totalBalance = balanceHandler.cacluateTotalBalance(wallets);

    balance.wallets = wallets;
    balance.totalBalance = totalBalance;

    commit('setBalances', balance);
  },
};
