export default () => ({
  address: '',
  isConnectedToMetamask: false,
  signatures: {
    user: null,
  },
  balance: {
    totalBalance: '',
    excessCollateral: null,
    collateralCoins: null,
    wallets: {},
  },
});
