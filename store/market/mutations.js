import BigNumber from 'bignumber.js';

export default {
  mergeRealCurrenciesWithFake(state, currencies) {
    console.log('currencies: ', currencies);
    const coins = currencies.map((currency) => {
      let usdValue = null;

      if (currency.price) {
        const priceBn = new BigNumber(currency.price);
        usdValue = priceBn.multipliedBy(currency.volume).toString();
      }

      currency.lastPrice = currency.price;
      currency.usdValue = usdValue;
      currency.tokens = currency.volume;
      currency.initial = currency.collateral ? `${currency.collateral} %` : currency.collateral;
      currency.maintenance = currency.maintainsCollateral ? `${currency.maintainsCollateral} %` : currency.maintainsCollateral;

      return currency;
    });
    state.tables.all.items = coins;
    state.tables.all.count = coins.length;

    state.tables.cryptoCurrency.items = coins;
    state.tables.cryptoCurrency.count = coins.length;
  },
};
