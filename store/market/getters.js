export default {
  getMarketTableAll: (state) => state.tables.all,
  getMarketTableCryptocurrency: (state) => state.tables.cryptoCurrency,
};
