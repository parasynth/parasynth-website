export default {
  updateCurrenciesTable({ commit }, currencies) {
    commit('mergeRealCurrenciesWithFake', currencies);
  },
};
