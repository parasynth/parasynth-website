export default {
  default: 'default',
  status: 'status',
  tooltip: 'tooltip',
  network: 'network',
  proposal: 'proposal',
  stake: 'stake',
  success: 'success',
  connectWallet: 'connectWallet',
  unstake: 'unstake',
};
