import { conversionVotingResultinPercent } from '~/utils/proposalHanlder';

export default {
  setSelectedProposal(state, proposal) {
    state.selectedProposal.proposalInfo = proposal.proposalInfo;
    state.selectedProposal.txHistory = proposal.users;
    state.selectedProposal.votingResults = proposal.votingResults;
  },
  updateSelectedProposal(state, proposal) {
    if (proposal.users) {
      state.selectedProposal.txHistory.push(proposal.users);
      state.selectedProposal.votingResults = conversionVotingResultinPercent(proposal.supportFor, proposal.supportAgainst);
    }

    const copy = JSON.parse(JSON.stringify(state.selectedProposal.proposalInfo));
    copy.status = proposal.status;
    state.selectedProposal.proposalInfo = copy;
  },
};
