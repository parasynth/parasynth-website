export default {
  getCurrencies: (state) => state.currencies,
  getSelectedProposalInfo: (state) => state.selectedProposal.proposalInfo,
  getSelectedProposalResult: (state) => state.selectedProposal.votingResults,
  getSelectedProposalTransactions: (state) => state.selectedProposal.txHistory,
};
