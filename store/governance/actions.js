import { finishVote, voteToProposal } from '~/plugins/web3';

export default {
  async getProposalById({ commit, rootGetters }, id) {
    const signature = rootGetters['wallet/getUserSignature'];
    const proposal = await this.$api.proposalService.getProposalById(id, signature);

    commit('setSelectedProposal', proposal);
  },
  async subscribeToProposalById({ commit }, id) {
    await this.$ws.subscribe(`/proposal/${id}/`, (message) => {
      commit('updateSelectedProposal', message);
    });
  },
  async unsubscribeToProposalById({ commit }, id) {
    await this.$ws.unsubscribe(`/proposal/${id}/`);
  },
  async voteToProposal({ commit }, { id, vote }) {
    return await voteToProposal(id, vote);
  },
  async finishVote({ commit }, id) {
    return await finishVote(id);
  },
};
