export default () => ({
  proposals: [],
  selectedProposal: {
    proposalInfo: {},
    txHistory: [],
    votingResults: {},
  },
});
