export default {
  getCurrencies: (state) => state.currencies,
  getAllTrades: ({ history }) => {
    const items = Object.values(history.allTrades.items).sort((a, b) => b.timestamp - a.timestamp);
    return {
      count: history.allTrades.count,
      items,
    };
  },
  getYourTrades: ({ history }) => {
    const items = Object.values(history.yourTrades.items).sort((a, b) => b.timestamp - a.timestamp);
    return {
      count: history.yourTrades.count,
      items,
    };
  },
  getYourOrders: ({ history }) => {
    const items = Object.values(history.yourOrders.items).sort((a, b) => b.timestamp - a.timestamp);
    return {
      count: history.yourOrders.count,
      items,
    };
  },
  getPairs: (state) => Object.values(state.pairs),
  getSelectedPair: (state) => state.selectedPair,
  getBarsApi: (state) => state.barsApi,
  getPollTime: (state) => state.pollTime,
  getPairStatusInfo: (state) => state.pairStatusInfo.history24h,
  getPairOrdersInfo: (state) => state.pairStatusInfo.orders,
  getRequiredCollateralInfo: (state) => state.requiredCollateralInfo,
  getServerTime: (state) => state.serverTime,
};
