import { CRYPTOCOMPARE_API_KEY, ORDER_STATUSES } from '~/utils/constants';
import orderHandler from '~/utils/orderHandler';

export default {
  async fetchCurrencies({ commit }) {
    const params = {
      api_key: CRYPTOCOMPARE_API_KEY,
      fsyms: 'BTC,ETH,DAI,BNB,LTC',
      tsyms: 'USD',
    };
    const response = await this.$axios.$get('https://min-api.cryptocompare.com/data/pricemultifull', { params });
    const currencies = {};

    Object.values(response.RAW).forEach(({ USD }) => {
      const res = {
        symbol: USD.FROMSYMBOL.toLowerCase(),
        price: USD.PRICE,
        volume24h: USD.VOLUME24HOUR,
        change24h: USD.CHANGE24HOUR,
        high24h: USD.HIGH24HOUR,
        low24h: USD.LOW24HOUR,
      };
      currencies[USD.FROMSYMBOL.toLowerCase()] = res;
    });
    commit('setFakeCurrencies', currencies);
  },

  // real implementation
  async createLimitOrder({ commit, rootGetters }, payload) {
    const res = await this.$api.orderService.createOrder(payload);

    const currencies = rootGetters['main/getCurrencies'];
    const newOrder = orderHandler.prepareOrder(res.result.order, currencies);
    console.log('newOrder: ', newOrder);

    commit('setNewOrder', newOrder);
  },
  async createMarketOrder({ commit, rootGetters }, payload) {
    const res = await this.$api.orderService.createOrder(payload);

    const currencies = rootGetters['main/getCurrencies'];
    const newOrder = orderHandler.prepareOrder(res.result.order, currencies);
    console.log('newOrder: ', newOrder);

    commit('setNewOrder', newOrder);
  },
  async fetchUserOrders({ commit, rootGetters }, { payload, params }) {
    if (!params) {
      params = {
        limit: 14,
        offset: 0,
      };
    }

    const orders = await this.$api.orderService.getUserOrders(payload, params);

    if (!Object.keys(orders.result).length) {
      return;
    }

    const currencies = rootGetters['main/getCurrencies'];

    if (typeof payload.ordersMatched === 'undefined') {
      const preparedUserOrders = orderHandler.prepareOrders(orders.result.ordersAll, currencies);
      const preparedUserTrades = orderHandler.prepareOrders(orders.result.ordersMatched, currencies);
      commit('setUserOrders', { orders: preparedUserOrders, count: orders.result.ordersAllCount });
      commit('setUserTrades', { orders: preparedUserTrades, count: orders.result.ordersMatchedCount });

      return;
    }

    if (payload.ordersMatched) {
      const preparedUserTrades = orderHandler.prepareOrders(orders.result.ordersMatched, currencies);
      commit('setUserTrades', { orders: preparedUserTrades, count: orders.result.ordersMatchedCount });
    } else {
      const preparedUserOrders = orderHandler.prepareOrders(orders.result.ordersAll, currencies);
      commit('setUserOrders', { orders: preparedUserOrders, count: orders.result.ordersAllCount });
    }
  },
  async updateUserOrders({ commit, rootGetters }, { payload, params }) {
    if (!params) {
      params = {
        limit: 14,
        offset: 0,
      };
    }

    const orders = await this.$api.orderService.getUserOrders(payload, params);
    const currencies = rootGetters['main/getCurrencies'];

    if (payload.ordersMatched) {
      const preparedUserTrades = orderHandler.prepareOrders(orders.result.ordersMatched, currencies);
      commit('mergeUserTrades', { orders: preparedUserTrades, count: orders.result.ordersMatchedCount });
    } else {
      const preparedUserOrders = orderHandler.prepareOrders(orders.result.ordersAll, currencies);
      commit('mergeUserOrders', { orders: preparedUserOrders, count: orders.result.ordersAllCount });
    }
  },
  async fetchAllTrades({ commit, rootGetters }) {
    const trades = await this.$api.orderService.getOrders();
    const currencies = rootGetters['main/getCurrencies'];
    const preparedAllTrades = orderHandler.prepareOrders(trades.result.orders, currencies);

    commit('setAllTrades', preparedAllTrades);
  },
  async fetchPairs({ commit }) {
    const pairs = await this.$api.pairService.getPairs();

    commit('setPairs', pairs);
  },
  async subscribeToUserOrderUpdates({ commit, rootGetters }) {
    await this.$ws.subscribe('/order/status/', (message) => {
      if (message.status === ORDER_STATUSES.ACCEPTED || message.status === ORDER_STATUSES.PARTIALLY_ACCEPTED) {
        const currencies = rootGetters['main/getCurrencies'];
        const preparedUpdatedOrder = orderHandler.prepareOrder(message.order, currencies);
        message.order = preparedUpdatedOrder;

        commit('updateUserOrders', message);
        return;
      }
      commit('updateUserOrders', message);
    });
  },
  async unsubscribeToUserOrderUpdates() {
    await this.$ws.unsubscribe('/order/status/');
  },
  async subscribeToPair({ commit }) {
    await this.$ws.subscribe('/pair/price/', (message) => {
      console.log('updatePair:', message);
      commit('updatePair', message);
    });
  },
  async unsubscribeToPair() {
    await this.$ws.unsubscribe('/pair/price/');
  },
  async getNextPollTime({ commit }) {
    const nextCycle = await this.$api.tradeService.getNextPollTime();
    commit('setServerTime', nextCycle);

    return nextCycle;
  },
  async subscribeToPollTimer({ commit }) {
    await this.$ws.subscribe('/order/poll/', (message) => {
      commit('setPollTime', message);
    });
  },
  async unsubscribeToPollTimer({ commit }) {
    await this.$ws.unsubscribe('/order/poll/');
  },
  async cancelOrder({ commit }, { order, signature }) {
    const res = await this.$api.orderService.cancelOrder(order.id, { signature });
    console.log('res: ', res);
  },
  async getPairStatusInfo({ commit }, pairId) {
    const pairStatusInfo = await this.$api.pairService.getPairStatusInfo(pairId);
    commit('setPairStatusInfo', pairStatusInfo);
  },
  async subscribeToPairStatusInfo({ commit }, pairId) {
    await this.$ws.subscribe(`/pair/${pairId}/`, (message) => {
      commit('updatePairStatusInfo', message);
    });
  },
  async unsubscribeToPairStatusInfo({ commit }, pairId) {
    await this.$ws.unsubscribe(`/pair/${pairId}/`);
  },
  async fetchPairById({ commit }, pairId) {
    const pair = await this.$api.pairService.getPairById(pairId);
    commit('setRequiredCollateralInfo', pair);
    return pair;
  },
};
