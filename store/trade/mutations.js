import BigNumber from 'bignumber.js';
import { ORDER_STATUSES } from '~/utils/constants';

export default {
  setFakeCurrencies(state, currencies) {
    state.currencies = currencies;
  },

  // NEW IMPLEMENTATION
  setNewOrder(state, newOrder) {
    const copyOrders = JSON.parse(JSON.stringify(state.history.yourOrders.items));
    // eslint-disable-next-line no-self-compare
    copyOrders[newOrder.id] = newOrder;

    state.history.yourOrders = {
      count: state.history.yourOrders.count + 1,
      items: copyOrders,
    };
    console.log('state your orders', state.history.yourOrders);
  },
  setUserOrders(state, userOrders) {
    state.history.yourOrders.items = userOrders.orders;
    state.history.yourOrders.count = userOrders.count;
  },
  mergeUserOrders(state, userOrders) {
    const copy = JSON.parse(JSON.stringify(state.history.yourOrders.items));
    Object.entries(userOrders.orders).reduce((acc, { key, value }) => {
      acc[key] = value;
      return acc;
    }, copy);

    state.history.yourOrders.items = copy;
    state.history.yourOrders.count = userOrders.count;
  },
  setUserTrades(state, userTrades) {
    state.history.yourTrades.items = userTrades.orders;
    state.history.yourTrades.count = userTrades.count;
  },
  mergeUserTrades(state, userTrades) {
    const copy = JSON.parse(JSON.stringify(state.history.yourTrades.items));
    Object.entries(userTrades.orders).reduce((acc, [key, value]) => {
      acc[key] = value;
      return acc;
    }, copy);

    state.history.yourTrades.items = copy;
    state.history.yourTrades.count = userTrades.count;
  },
  setAllTrades(state, allTrades) {
    state.history.allTrades.items = allTrades;
    state.history.allTrades.count = Object.keys(allTrades).length;
  },
  updateUserOrders(state, orderInfo) {
    const updatedOrder = JSON.parse(JSON.stringify(orderInfo));

    if (updatedOrder.status === ORDER_STATUSES.PARTIALLY_ACCEPTED || updatedOrder.status === ORDER_STATUSES.ACCEPTED) {
      const copyYourOrders = JSON.parse(JSON.stringify(state.history.yourOrders.items));
      const copyYourTrades = JSON.parse(JSON.stringify(state.history.yourTrades.items));
      const copyAllTrades = JSON.parse(JSON.stringify(state.history.allTrades.items));
      const userOrder = copyYourOrders[updatedOrder.orderId];

      updatedOrder.order.executionPrice = updatedOrder.execution_price;
      updatedOrder.order.commission = updatedOrder.commission;

      if (typeof userOrder === 'undefined') {
        // set all trades
        copyAllTrades[updatedOrder.orderId] = updatedOrder.order;
        state.history.allTrades.items = copyAllTrades;
        state.history.allTrades.count = copyAllTrades.length;
        return;
      }

      // remove accepted order from your orders
      delete copyYourOrders[updatedOrder.orderId];
      state.history.yourOrders.items = copyYourOrders;
      state.history.yourOrders.count = copyYourOrders.length;

      // set your trades
      copyYourTrades[updatedOrder.orderId] = updatedOrder.order;
      state.history.yourTrades.items = copyYourTrades;
      state.history.yourTrades.count = copyYourTrades.length;

      // set all trades
      copyAllTrades[updatedOrder.orderId] = updatedOrder.order;
      state.history.allTrades.items = copyAllTrades;
      state.history.allTrades.count = copyAllTrades.length;
    } else {
      const userOrder = state.history.yourOrders.items[updatedOrder.orderId];

      if (typeof userOrder === 'undefined') return;

      const amount = new BigNumber(userOrder.amount);
      userOrder.status = updatedOrder.status;

      if (updatedOrder.amount > userOrder.executed) {
        userOrder.executed = updatedOrder.amount;
        userOrder.amount = amount.minus(updatedOrder.amount).toString();
      }
    }
  },
  setPairs(state, pairs) {
    const pairsObj = pairs.reduce((acc, pair) => {
      acc[pair.id] = pair;
      return acc;
    }, {});

    state.pairs = pairsObj;
  },
  updatePair(state, pair) {
    const copyStatusInfo = JSON.parse(JSON.stringify(state.pairStatusInfo));
    copyStatusInfo.history24h.price = pair.price;
    state.pairStatusInfo = copyStatusInfo;

    state.pairs[pair.pairId].lastPrice = pair.price;
    state.pairs[pair.pairId].updatedAt = pair.updatedAt;
  },
  setSelectedPair(state, pair) {
    state.selectedPair = pair;
  },
  setPollTime(state, pollTime) {
    state.pollTime = pollTime.createdAt;
  },
  setPairStatusInfo(state, pairInfo) {
    state.pairStatusInfo.history24h = pairInfo.history24h;
    state.pairStatusInfo.orders = pairInfo.orders;
  },
  updatePairStatusInfo(state, pairInfo) {
    state.pairStatusInfo.history24h = pairInfo.history24h;
    state.pairStatusInfo.orders = pairInfo.orders;
  },
  setRequiredCollateralInfo(state, pair) {
    const requiredCollateralInfo = {};

    requiredCollateralInfo.pairCollateral = pair.pairCollateral;
    requiredCollateralInfo.priceBase = pair.priceBase;
    requiredCollateralInfo.priceQuote = pair.priceQuote;

    state.requiredCollateralInfo = requiredCollateralInfo;
  },
  setServerTime(state, serverTime) {
    state.serverTime = serverTime;
  },
};
