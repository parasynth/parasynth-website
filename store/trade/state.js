export default () => ({
  currencies: {},
  history: {
    yourOrders: {
      count: 0,
      items: {},
    },
    yourTrades: {
      count: 0,
      items: {},
    },
    allTrades: {
      count: 0,
      items: {},
    },
  },
  pairs: {},
  selectedPair: null,
  pollTime: '',
  serverTime: '',
  pairStatusInfo: {
    history24h: {},
    orders: {},
  },
  requiredCollateralInfo: null,
});
