import localeEn from './locales/en.json';
import dateTimeFormats from './locales/dateTimeFormats.js';
import numberFormats from './locales/numberFormats.js';

require('dotenv').config();

export default {
  ssr: false,
  target: 'static',
  head: {
    title: 'Omnisynth',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'msapplication-TileColor', content: '#ffffff' },
      { name: 'msapplication-TileColor', content: '/ms-icon-144x144.png' },
      { name: 'theme-color', content: '#ffffff' },
    ],
    link: [
      {
        rel: 'apple-touch-icon', sizes: '57x57', href: '/apple-icon-57x57.png',
      },
      {
        rel: 'apple-touch-icon', sizes: '60x60', href: '/apple-icon-60x60.png',
      },
      {
        rel: 'apple-touch-icon', sizes: '72x72', href: '/apple-icon-72x72.png',
      },
      {
        rel: 'apple-touch-icon', sizes: '114x114', href: '/apple-icon-114x114.png',
      },
      {
        rel: 'apple-touch-icon', sizes: '120x120', href: '/apple-icon-120x120.png',
      },
      {
        rel: 'apple-touch-icon', sizes: '144x144', href: '/apple-icon-144x144.png',
      },
      {
        rel: 'apple-touch-icon', sizes: '152x152', href: '/apple-icon-152x152.png',
      },
      {
        rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-icon-180x180.png',
      },
      {
        rel: 'icon', type: 'image/png', sizes: '192x192', href: '/android-icon-192x192.png',
      },
      {
        rel: 'icon', type: 'image/png', sizes: '32x32', href: '/android-icon-32x32.png',
      },
      {
        rel: 'icon', type: 'image/png', sizes: '96x96', href: '/android-icon-96x96.png',
      },
      {
        rel: 'icon', type: 'image/png', sizes: '16x16', href: '/android-icon-16x16.png',
      },
      {
        rel: 'manifest', href: '/manifest.json',
      },
    ],
  },
  css: [
    '@/assets/scss/main.scss',
  ],
  plugins: [
    { src: '@plugins/api.js' },
    { src: '@plugins/ws', mode: 'client' },
    { src: '@plugins/main.js' },
    { src: '@plugins/init.js', mode: 'client' },
    { src: '@plugins/vee-validate.js' },
    { src: '@plugins/injectComponents.js' },
    { src: '@plugins/datafeed', ssr: false },
    { src: '@plugins/charting-library', mode: 'client' },
    { src: '@plugins/web3.js', mode: 'client' },
    { src: '~plugins/v-mask', ssr: false },
  ],
  loading: '~/components/ui/app-loading/loading.vue',
  loadingIndicator: '~/assets/loader.html',
  components: true,
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/dotenv',
    '@nuxtjs/color-mode',
  ],
  styleResources: {
    scss: ['./assets/scss/resourses.scss'],
  },
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    'bootstrap-vue/nuxt',
    'nuxt-i18n',
    'cookie-universal-nuxt',
    'vue-currency-input/nuxt',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: process.env.BASE_URL,
  },
  build: {
    transpile: [
      'vee-validate/dist/rules',
    ],
  },
  // bootstrapVue: {
  //   // bootstrapCSS: false, // Or `css: false`
  //   // bootstrapVueCSS: false, // Or `bvCSS: false`
  //   icons: true,
  // },
  i18n: {
    locales: ['en'],
    defaultLocale: 'en',
    strategy: 'no_prefix',
    vueI18n: {
      messages: {
        en: localeEn,
      },
      dateTimeFormats,
      numberFormats,
    },
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected',
      alwaysRedirect: true,
    },
  },
  env: {
    BASE_URL: process.env.BASE_URL,
    WS_URL: process.env.WS_URL,
    CHAIN_ID: process.env.CHAIN_ID,
    INFURA: process.env.INFURA,
    SWAP_CONTRACT_ADDRESS: process.env.SWAP_CONTRACT_ADDRESS,
    STAKE_CONTRACT_ADDRESS: process.env.STAKE_CONTRACT_ADDRESS,
    DAO_CONTRACT_ADDRESS: process.env.DAO_CONTRACT_ADDRESS,
    COINGECKO_URL: process.env.COINGECKO_URL,
  },
};
